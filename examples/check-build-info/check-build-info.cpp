// Source Code: https://docs.opencv.org/3.4.6/d4/d26/samples_2cpp_2facedetect_8cpp-example.html
// Small edits made by modalai.com

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;


int main( int argc, const char** argv )
{

  std::cout << cv::getBuildInformation() << std::endl;
  return 0;

}
